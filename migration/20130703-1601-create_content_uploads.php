<?php

function install() {
	db()->query("CREATE TABLE IF NOT EXISTS `content_upload` (
			`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
			`category` varchar(200) NOT NULL,
			`value` varchar(200) NOT NULL,
			`name` varchar(200) NOT NULL,
			`path` varchar(200) NOT NULL,
			`create_by` int(10) unsigned DEFAULT NULL,
			`create_date` int(10) unsigned DEFAULT NULL,
			PRIMARY KEY (`id`),
			UNIQUE KEY `path` (`path`),
			KEY `category` (`category`(50), `value`(20))
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
}

function remove() {
	db()->query("DROP TABLE `content_upload`;");
}
