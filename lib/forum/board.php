<?php

class forum_board {
	public $id;
	public $name;
	public $description;
	public $prio;
	public $lft;
	public $rgt;
	public $parent;
	public $public_read;
	public $public_write;
	public $public_reply;
	public $last_thread;
	public $last_post;
	public $count_posts;
	public $count_threads;
	public $create_date;
	public $create_by;
	public $update_date;
	public $update_by;

	public function breadcrumb() {
		return db()->query("
			SELECT parent.*
			FROM forum_board AS node, forum_board AS parent
			WHERE node.lft BETWEEN parent.lft AND parent.rgt  AND node.id = %d
			ORDER BY node.lft;", $this->id );
	}

	public function update() {
		$data = db()->query("
			SELECT MAX(p.id) AS last_post, count(*) AS count_posts
			FROM forum_post p
			JOIN forum_thread t ON p.thread = t.id
			WHERE t.board = %d", $this->id )->assoc();

		$data['last_thread'] = db()->query("SELECT thread FROM forum_post WHERE id = %d", $data['last_post'])->value();
		$data['count_threads'] = db()->query("SELECT count(*) FROM forum_thread WHERE board = %d", $this->id)->value();

		db()->forum_board->updateRow($data, $this->id);
	}

	public function write( $title, $text ) {
		db()->forum_thread->insert(array(
				'board' => $this->id,
				'title' => $title,
		));

		$tid = db()->id();
		$thread = forum_thread::create( $tid, $this);
		$thread->reply( $title, $text );
		return $tid;
	}

	public function getBoards() {
		/** @var rights_container $rights */
		$rights = iv::get('rights');
		$user = iv::get('user');

		// load sub boards
		$result = db()->query("
				SELECT b.*,
					t.id AS last_thread_id,
					t.title AS last_thread_title,
					p.create_date AS last_post_date,
					u.id AS last_post_user_id,
					u.name AS last_post_user_name
				FROM forum_board b
				LEFT JOIN forum_thread t ON b.last_thread = t.id
				LEFT JOIN forum_post p ON b.last_post = p.id
				LEFT JOIN user_data u ON p.create_by = u.id
				WHERE b.parent = %d", $this->id )->objects();

		$unread = $user ? db()->query("SELECT DISTINCT t.board AS id
				FROM forum_unread u
				JOIN forum_thread t ON t.id = u.thread
				WHERE u.create_by = %d", $user->id)->relate('id'): array();

		foreach( $result as $i => $sub )
			if( !$sub->public_read && !$rights->has('forum', $sub->id))
				unset( $result[$i] );
			else
				$sub->new = $user && ( $user->last_read < $sub->last_post || isset( $unread[$sub->id] ));

		return $result;
	}

	public function getThreads() {
		$user = iv::get('user');

		$threads = db()->query("
				SELECT t.*,
					p.create_date as last_post_date,
					u.id as last_post_user_id,
					u.name as last_post_user_name
				FROM forum_thread t
				LEFT JOIN forum_post p ON t.last_post = p.id
				LEFT JOIN user_data u ON p.create_by = u.id
				WHERE board = %d
				ORDER BY last_post DESC", $this->id )->objects();

		$unread = $user ? db()->query("SELECT t.id
				FROM forum_unread u
				JOIN forum_thread t ON t.id = u.thread
				WHERE u.create_by = %d AND t.board = %d",
				$user->id, $this->id )->relate('id'): array();

		foreach( $threads as $thread )
			$thread->new = $user && ( $user->last_read < $thread->last_post || isset( $unread[$thread->id] ));

		return $threads;
	}

	public function checkRights( $type ) {
		/** @var rights_container $rights */
		$rights = iv::get('rights');
		$user = iv::get('user');

		$flags = $rights->flags('forum', $this->id);
		$prop = 'public_'.$type;

		if( !$user || (!$this->{$prop} && !$flags[$type])) {
			throw new Exception('Sie haben keinen Zugriff auf dieses Forum!');
		}
	}

	/**
	 * @param int $id
	 * @return forum_board
	 * @throws Exception
	 */
	public static function load($id) {
		if( !$board = db()->forum_board->row( $id )->object( 'forum_board' ))
			throw new Exception('Forum nicht gefunden!');
		return $board;
	}
} 