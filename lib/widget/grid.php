<?php

/*
For bootstrap grid
Didn't worked very well
Constructor part:
		foreach( func_get_args() as $width ) {
			$col = $this->columns[] = new html_tag('div');
			$col->class = 'span'.$width;
		}

To-String part:
		$result = '<div class="grid">'; // row-fluid
		foreach( $this->columns as $col ) $result .= $col;
		return $result . '</div>';

If you have to specify columns in constructor
	public function __construct( $columns ) {
		for( $i=0; $i<$columns; $i++)
			$this->columns[] = new html_tag('div');
	}
 */

class grid_column extends html_tag {
	public function __construct() {
		parent::__construct('div');
	}

	public function box( $content, $title = "Content Box", $width = NULL ) {
		return $this->append( new widget_box( $content, $title, $width ));
	}

	public function add( $data ) {
		$this->append( $data );
	}
}

class widget_grid extends html_tag implements ArrayAccess {
	public function __construct() {
		parent::__construct('div');
		$this->class = 'grid';
	}

	public function offsetExists( $offset ) {
		return isset( $this->children[$offset] );
	}

	public function offsetGet( $offset ) {
		if( isset( $this->children[$offset] )) return $this->children[$offset];
		else	return $this->children[$offset] = new grid_column();
	}

	public function offsetSet( $offset, $value ) {
		return false;
	}

	public function offsetUnset( $offset ) {
		return false;
	}
}
