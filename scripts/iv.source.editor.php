<?php

// @TODO: Make this clean

$syntax = $panel['script'] == 'html' ? 'htmlmixed' : 'text/x-php';
if( isset( $_POST['source'] ))
	$panelvars['content'] = $_POST['source'];

?>
<div class="box">
	<h2>Panel bearbeiten: <?php echo htmlspecialchars( $panel['name']); ?></h2>
	<div style="padding:0; margin:0;">
		<form action="<?php echo EDITOR_SELF; ?>" name="editor" method="post">
			<script type="text/javascript" src="assets/codemirror/lib/codemirror.js"></script>
			<script type="text/javascript" src="assets/codemirror/mode/xml/xml.js"></script>
			<script type="text/javascript" src="assets/codemirror/mode/javascript/javascript.js"></script>
			<script type="text/javascript" src="assets/codemirror/mode/css/css.js"></script>
			<script type="text/javascript" src="assets/codemirror/mode/htmlmixed/htmlmixed.js"></script>
			<script type="text/javascript" src="assets/codemirror/mode/clike/clike.js"></script>
			<script type="text/javascript" src="assets/codemirror/mode/php/php.js"></script>
			<link rel="stylesheet" href="assets/codemirror/lib/codemirror.css">
			<script language="javascript" type="text/javascript">window.onload = function(){ CodeMirror.fromTextArea(document.getElementById("source"), { lineNumbers: true, mode: "<?php echo $syntax; ?>", tabSize:4, indentWithTabs:true, autofocus:true }); };</script>

			<textarea style=" width:100%;" rows="30" name="source" id="source"><?php echo htmlspecialchars( $panelvars['content'] ); ?></textarea>
			<p align="center" style="border-top:1px solid #ddd; padding-top:15px;">
				<input type="submit" value="Speichern" class="btn btn-primary">
				<a href="<?php echo LAYER_SELF; ?>" class="btn">Abbrechen</a>
			</p>
		</form>
	</div>
</div>
